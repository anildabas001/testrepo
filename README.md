## Steps to run the index.html
1.Download the file on your pc.
2.Double click on the file or open it with google chrome.
3.To edit the file open it in notepad or any text editor.

## Reasons to choose MIT license
1.It is most popular license used for repositories.
2.MIT license is open source friendly.
3.It allows anyone to do anything with your project like using, modifying, distributing etc.
4.Many popular open source projects like .NET Core, Bable and Rails are using this license.

## How to add license to BitBucket repository
just create a new file copy the MIT license content and paste the content in your newly created file and give it a name license or License.txt

## File Changed++